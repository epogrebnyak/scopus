import numpy as np
import pandas as pd
import statsmodels.formula.api as sm

df = pd.read_csv("raw.csv").dropna()
df = df["title coverage_start sjr".split()].drop_duplicates()
df["coverage_start"] = df["coverage_start"].map(int)
df["age"] = 2020 - df["coverage_start"]

# Are older journals more cited?
df.plot.scatter(
    x="coverage_start",
    y="sjr",
    logy=True,
    title="Are older journals more cited?\nhttps://gitlab.com/epogrebnyak/scopus",
    alpha=0.3,
)

# Distribution of years coverage started
zf = df[["sjr", "coverage_start"]].groupby("coverage_start").count()
zf.plot.bar()


df.plot.scatter(
    x="age",
    y="sjr",
    logy=True,
    title="Are older journals more cited?\nhttps://gitlab.com/epogrebnyak/scopus",
    alpha=0.3,
)

df["sjr_log"] = np.log(df.sjr)
result = sm.ols(formula="sjr_log ~ age", data=df).fit()
print(result.summary())

# import matplotlib.pyplot as plt
# plt.plot(x, y, '.')
# plt.plot(x, b + m * x, '-')
# plt.show()
