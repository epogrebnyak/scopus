import os
from dataclasses import dataclass
from typing import List, Optional, Union

import requests
import requests_cache  # type: ignore
from dotenv import load_dotenv

requests_cache.install_cache("else")
load_dotenv("key.txt")
API_KEY = os.getenv("SCOPUS_API_KEY")


def get_page(url: str):
    return requests.get(url)


def get_scopus_response(url: str):
    return get_page(url).json()["serial-metadata-response"]


def mk_url(issn, api_key=API_KEY):
    from urllib.parse import urlencode

    param = urlencode([("issn", issn), ("apiKey", api_key)])
    return "http://api.elsevier.com/content/serial/title?" + param


@dataclass
class Journal:
    issn: Optional[str]
    e_issn: Optional[str]
    title: str
    publisher: str
    coverage_start: Optional[int]
    subject_abbr: List[str]
    subject_areas: List[str]
    subject_codes: List[str]
    sjr: Optional[float]
    sjr_year: Optional[int]


@dataclass
class Response:
    issn: Union[str, int]

    def __post_init__(self):
        self.issn = str(self.issn).zfill(8)
        self.json = get_scopus_response(mk_url(self.issn))

    @property
    def link(self):
        return self.json["link"]

    @property
    def entry(self):
        try:
            return self.json["entry"][0]
        except (KeyError, IndexError):
            return dict()


def safer(f):
    def safe(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except (KeyError, IndexError, TypeError):
            return None

    return safe


@safer
def coverage_start(r: Response):
    return int(r.entry["coverageStartYear"])


@safer
def title(r: Response):
    return r.entry["dc:title"]


@safer
def publisher(r: Response):
    return r.entry["dc:publisher"]


@safer
def sjr_last(r: Response):
    xs = r.entry["SJRList"]["SJR"][-1]
    year = int(xs["@year"])
    ranking = float(xs["$"])
    return year, ranking


@safer
def sjr_year(r: Response):
    return sjr_last(r)[0]


@safer
def sjr_ranking(r: Response):
    return sjr_last(r)[1]


@safer
def prism_issn(r: Response):
    return r.entry["prism:issn"]


@safer
def prism_electronic_issn(r: Response):
    return r.entry["prism:eIssn"]


def unique(xs):
    return list(set(xs))


@safer
def subject_abbr(r: Response):
    return unique([x["@abbrev"] for x in r.entry["subject-area"]])


@safer
def subject_areas(r: Response):
    return [x["$"] for x in r.entry["subject-area"]]


@safer
def subject_codes(r: Response):
    return [x["@code"] for x in r.entry["subject-area"]]


def to_journal(r: Response) -> Journal:
    return Journal(
        issn=prism_issn(r),
        e_issn=prism_electronic_issn(r),
        title=title(r),
        publisher=publisher(r),
        sjr_year=sjr_year(r),
        sjr=sjr_ranking(r),
        coverage_start=coverage_start(r),
        subject_abbr=subject_abbr(r),
        subject_areas=subject_areas(r),
        subject_codes=subject_codes(r),
    )


def get_journal(issn: Union[str, int]) -> Journal:
    return to_journal(Response(issn))


Journal(
    issn="0022-3808",
    e_issn="1537-534X",
    title="Journal of Political Economy",
    publisher="University of Chicago",
    coverage_start=1969,
    subject_abbr=["ECON"],
    subject_areas=["Economics and Econometrics"],
    subject_codes=["2002"],
    sjr=21.239,
    sjr_year=2019,
)
