scopus
======

A lightweight Python client for [Scopus API](https://dev.elsevier.com/).

To use: [obtain an API key](https://dev.elsevier.com/apikey/manage) 
        and save it in `key.txt` as

```
SCOPUS_API_KEY="your key here"
```

Minimal example:

```python
from scopus import get_journal

j = get_journal("223808")
assert j == Journal(
    issn="0022-3808",
    e_issn="1537-534X",
    title="Journal of Political Economy",
    publisher="University of Chicago",
    coverage_start=1969,
    subject_abbr=["ECON"],
    subject_areas=["Economics and Econometrics"],
    subject_codes=["2002"],
    sjr=21.239,
    sjr_year=2019,
)
```

Alternatives:

- https://pybliometrics.readthedocs.io/en/stable/
- https://github.com/dhimmel/scopus

