from scopus import Response, get_journal

r = Response("00223808")
j = get_journal("00223808")


def test_get_journal():
    assert j.issn == "0022-3808"
    assert j.e_issn == "1537-534X"
    assert j.title == "Journal of Political Economy"
    assert j.publisher == "University of Chicago"
    assert j.sjr == 21.239
    assert j.sjr_year == 2019
    assert j.coverage_start == 1969
    assert j.subject_abbr == ["ECON"]
    assert j.subject_areas == ["Economics and Econometrics"]
    assert j.subject_codes == ["2002"]


def test_allow_dash():
    assert get_journal("0022-3808") == get_journal("00223808")


def test_has_no_electronic_issn():
    assert get_journal("00028282").e_issn is None


def test_multiple_subject_areas():
    r = Response("00223808")
    assert r.title == "Journal of Finance"
    assert r.subject_abbr == ["ECON", "BUSI"]
    assert r.subject_areas == ["Accounting", "Finance", "Economics and Econometrics"]
    assert r.subject_codes == ["1402", "2003", "2002"]
